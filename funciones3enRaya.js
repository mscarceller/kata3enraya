/* 
    Document   : funciones3enRaya
    Created on : 13-nov-2012
    Author     : mscarceller
    Description:
        Funciones javascript de la kata 3 en raya
*/


var turno = "Jugador 1";
var maquina = false;
var size  = 0;
var nCols = 0;
var nRows = 0;
var numJugadas  = 0;
var finDelJuego = false;

/**
* Funcion auxiliar que impide la entrada de letras como tamaño del tablero
*/
function soloNumeros(evt){
    var tecla = (evt.which) ? evt.which : event.keyCode
    if (tecla > 31 && (tecla < 48 || tecla > 57))
        return false;
    return true;
}

/**
* Funcion que inicia un juego
*/
function initGame(){
    size = document.getElementById('size').value;
    if (size==""){
        alert("Introduzca el tama&ntilde;o del tablero!");
        return; 
    }
    if (size<3){
        alert("El tama&ntilde;o debe ser mayor que 2!");
        return; 
    }
    if (document.getElementById('maquina').checked)
        maquina = true;

    finDelJuego = false;
    nCols = size;
    nRows = size;
    numJugadas=0;
    document.getElementById('turno').innerHTML="";
    turno = "Jugador 1";
    document.getElementById('turno').innerHTML = "Turno Jugador 1";
    document.getElementById('tablero').innerHTML ="";
    creaTablero();

}

/**
* Funcion que crea 1 tablero de juego
*/
function creaTablero(){
    var row;
    var cell;
    var tablero = document.getElementById('tablero');
    var tabla   = document.createElement('table');
    var tbody   = document.createElement('tbody');
    tabla.className="tabla";
    for(var i = 0 ; i < nRows ; i++){
        row = document.createElement('tr');
        for(var j = 0 ; j < nCols ; j++){
            cell = creaCasilla(i,j);
            row.appendChild(cell);
        }
        tbody.appendChild(row);
    }
    tabla.appendChild(tbody);
    tablero.appendChild(tabla);
}

/**
* Funcion que crea 1 casilla con id = coordenadas
*/
function creaCasilla(i,j){
    var Casilla= document.createElement('td');
    Casilla.setAttribute('name',i+'_'+j);
    Casilla.setAttribute('id',i+'_'+j);
    Casilla.setAttribute('value','  ');
    Casilla.onclick = click;
    return Casilla;
}

/**
* Funcion que procesa un click sobre el tablero
*/
function click(){
    if (finDelJuego){
        alert("El juego ya ha terminado. Comienza uno nuevo!");
        return;
    }

    if (this.innerHTML!=""){
        alert("Casilla ocupada!");
        return;
    }
    numJugadas++;
    if (turno == "Jugador 1"){
        this.className = "tdX";
        this.innerHTML = "X";
        if (buscaGanador('X')){
            document.getElementById('turno').innerHTML = "Fin del Juego: Gana Jugador 1!!";
            alert("Fin del Juego: Gana Jugador 1!!");
            finDelJuego=true;
            return;
        }

        turno = "Jugador 2";
        if(maquina){
            document.getElementById('turno').innerHTML = "Turno M&aacute;quina";
            turnoMaquina();
        }
        else
            document.getElementById('turno').innerHTML = "Turno Jugador 2";           
    }
    else{
        this.className = "tdO";
        this.innerHTML = "O";
        if (buscaGanador('O')){
            if(!maquina){
                document.getElementById('turno').innerHTML = "Fin del Juego: Gana Jugador 2!!";
                alert("Fin del Juego: Gana Jugador 2!!");
            }
            else{
                document.getElementById('turno').innerHTML = "Fin del Juego: Gana la Maquina!!";
                alert('Fin del Juego: Gana la Maquina!!');
            }
            finDelJuego=true;
            return;
        }
        turno = "Jugador 1";
        document.getElementById('turno').innerHTML = "Turno Jugador 1";             
    }
    if (numJugadas==(size*size)){
        document.getElementById('resultado').innerHTML = "Fin del Juego: EMPATE!!";
        finDelJuego=true;
        return;
    }
}

/**
* Funcion que chequa si alguien ha ganado
*/
function buscaGanador(turno){
        var nNRaya = 0;
        for (i = 0; i < nRows; i++){
        for (j = 0; j < nCols; j++){
            nNRaya = 0;
            casilla = i+'_'+j;
            valorCasilla = document.getElementById(casilla).innerHTML;
            if (valorCasilla == turno){
            /**********      evaluamos las diagonales    **************/
            if (i-1 >= 0  && j-1 >= 0){
                if (document.getElementById((i-1)+"_"+(j-1)).innerHTML == turno){
                    nNRaya++;  
                    if (i-2 >= 0  && j-2 >= 0){
                        if (document.getElementById((i-2)+"_"+(j-2)).innerHTML == turno)
                            nNRaya++;  
                    }
                }      
            }
            if (nNRaya > 1) return true;
            nNRaya = 0; 
            if (i+1 < nRows  && j+1 < nCols){
                if (document.getElementById((i+1)+"_"+(j+1)).innerHTML == turno){
                    nNRaya++;  
                    if (i+2 < nRows  && j+2 < nCols){
                        if (document.getElementById((i+2)+"_"+(j+2)).innerHTML == turno)
                        nNRaya++; 
                    }
                }
            } 
            if (nNRaya > 1) return true;
            nNRaya = 0;
            if (i-1 >= 0  && j+1 < nCols){
                if (document.getElementById((i-1)+"_"+(j+1)).innerHTML == turno){
                    nNRaya++;  
                    if (i-2 >= 0  && j+2 < nCols){
                        if (document.getElementById((i-2)+"_"+(j+2)).innerHTML == turno)
                            nNRaya++;  
                    }
                }      
            }
            if (nNRaya > 1) return true;
            nNRaya = 0;
            if (i+1 < nRows  && j-1 >= 0){
                if (document.getElementById((i+1)+"_"+(j-1)).innerHTML == turno){
                    nNRaya++;  
                    if (i+2 < nRows  && j-2 >= 0){
                        if (document.getElementById((i+2)+"_"+(j-2)).innerHTML == turno)
                        nNRaya++; 
                    }
                }
            } 
            if (nNRaya > 1) return true;
            nNRaya = 0;

            /******   Evaluamos en horizontal y vertical      *********/
            if (i-1 >= 0){
                if (document.getElementById((i-1)+"_"+(j)).innerHTML == turno){
                    nNRaya++;  
                    if (i-2 >= 0){
                        if (document.getElementById((i-2)+"_"+(j)).innerHTML == turno)
                            nNRaya++;  
                    }
                }
            }
            if (nNRaya > 1) return true;
            nNRaya = 0;
            if (i+1 < nRows){
                if (document.getElementById((i+1)+"_"+(j)).innerHTML == turno){
                    nNRaya++;  
                    if (i+2 < nRows){
                        if (document.getElementById((i+2)+"_"+(j)).innerHTML == turno)
                            nNRaya++; 
                    }
                }
            }
            if (nNRaya > 1) return true;
            nNRaya = 0;
            if (j-1 >= 0){
                if (document.getElementById((i)+"_"+(j-1)).innerHTML == turno){
                    nNRaya++;  
                    if (j-2 >= 0){
                        if (document.getElementById((i)+"_"+(j-2)).innerHTML == turno)
                            nNRaya++;  
                    }
                }
            }
            if (nNRaya > 1) return true;
            nNRaya = 0;
            if (j+1 < nCols){
                if (document.getElementById((i)+"_"+(j+1)).innerHTML == turno){
                    nNRaya++;  
                    if (j+2 < nCols){
                        if (document.getElementById((i)+"_"+(j+2)).innerHTML == turno)
                            nNRaya++; 
                    }
                }
            }
            if (nNRaya > 1) return true;
            nNRaya = 0;
            }
        }
        }
        return false;
}

/**
* Funcion que procesa un turno de la maquina en 3 pasos
*/
function turnoMaquina(){
    //primera pasada de bloqueo
    //segunda pasada intenta ganar
    //tercera pasada coloca ficha donde puede
    for (b = 0; b < 3; b++){
        if (b==0)      
            turno   = 'X'; 
        else
            turno   = 'O';
        for (i = 0; i < nRows; i++){
            for (j = 0; j < nCols; j++){
                valorCasilla = document.getElementById(i+'_'+j).innerHTML;
                if ((valorCasilla == "" && b == 2)){
                    document.getElementById((i)+"_"+(j)).click();
                    return true; 
                }
                if (valorCasilla == turno && b < 2){                      
                /**********      evaluamos las diagonales    **********/
                    if (i-1 >= 0  && j-1 >= 0){
                        if (document.getElementById((i-1)+"_"+(j-1)).innerHTML == turno){
                            if (i-2 >= 0  && j-2 >= 0){
                                if (document.getElementById((i-2)+"_"+(j-2)).innerHTML == ""){
                                    document.getElementById((i-2)+"_"+(j-2)).click();
                                    return true;
                                }       
                            }     
                        }
                        else if (document.getElementById((i-1)+"_"+(j-1)).innerHTML == "" && b == 1){
                                document.getElementById((i-1)+"_"+(j-1)).click();
                                return true;
                        }
                    }
                    if (i+1 < nRows  && j+1 < nCols){
                        if (document.getElementById((i+1)+"_"+(j+1)).innerHTML == turno){
                            if (i+2 < nRows  && j+2 < nCols){
                                if (document.getElementById((i+2)+"_"+(j+2)).innerHTML == ""){
                                    document.getElementById((i+2)+"_"+(j+2)).click();
                                    return true;
                                }       
                            }     
                        }
                        else if (document.getElementById((i+1)+"_"+(j+1)).innerHTML == "" && b == 1){
                                document.getElementById((i+1)+"_"+(j+1)).click();
                                return true;
                        }
                    }
                    if (i-1 >= 0  && j+1 < nCols){
                        if (document.getElementById((i-1)+"_"+(j+1)).innerHTML == turno){
                            if (i-2 >= 0  && j+2 < nCols){
                                if (document.getElementById((i-2)+"_"+(j+2)).innerHTML == ""){
                                    document.getElementById((i-2)+"_"+(j+2)).click();
                                    return true;
                                }       
                            }     
                        }
                        else if (document.getElementById((i-1)+"_"+(j+1)).innerHTML == "" && b == 1){
                                document.getElementById((i-1)+"_"+(j+1)).click();
                                return true;
                        }
                    }
                    if (i+1 < nRows  && j-1 >= 0){
                        if (document.getElementById((i+1)+"_"+(j-1)).innerHTML == turno){
                            if (i+2 < nRows  && j-2 >= 0){
                                if (document.getElementById((i+2)+"_"+(j-2)).innerHTML == ""){
                                    document.getElementById((i+2)+"_"+(j-2)).click();
                                    return true;
                                }       
                            }     
                        }
                        else if (document.getElementById((i+1)+"_"+(j-1)).innerHTML == "" && b == 1){
                                document.getElementById((i+1)+"_"+(j-1)).click();
                                return true;
                        }
                    }

                    /******    evaluamos horizaontal y vertical   *****/
                    if (i-1 >= 0){
                        if (document.getElementById((i-1)+"_"+(j)).innerHTML == turno){
                            if (i-2 >= 0){
                                if (document.getElementById((i-2)+"_"+(j)).innerHTML == ""){
                                    document.getElementById((i-2)+"_"+(j)).click();
                                    return true;
                                }
                            }     
                        }
                        else if (document.getElementById((i-1)+"_"+(j)).innerHTML == "" && b == 1){
                                document.getElementById((i-1)+"_"+(j)).click();
                                return true;
                        }
                    }
                    if (i+1 < nRows){
                        if (document.getElementById((i+1)+"_"+(j)).innerHTML == turno){
                            if (i+2 < nRows){
                                if (document.getElementById((i+2)+"_"+(j)).innerHTML == ""){
                                    document.getElementById((i+2)+"_"+(j)).click();
                                    return true;
                                }
                            }     
                        }
                        else if (document.getElementById((i+1)+"_"+(j)).innerHTML == "" && b == 1){
                                document.getElementById((i+1)+"_"+(j)).click();
                                return true;
                        }
                    }

                    if (j-1 >= 0){
                        if (document.getElementById((i)+"_"+(j-1)).innerHTML == turno){
                            if (j-2 >= 0){
                                if (document.getElementById((i)+"_"+(j-2)).innerHTML == ""){
                                    document.getElementById((i)+"_"+(j-2)).click();
                                    return true;
                                }
                            }     
                        }
                        else if (document.getElementById((i)+"_"+(j-1)).innerHTML == "" && b == 1){
                                document.getElementById((i)+"_"+(j-1)).click();
                                return true;
                        }
                    }
                    if (j+1 < nCols){
                        if (document.getElementById((i)+"_"+(j+1)).innerHTML == turno){
                            if (i+2 < nCols){
                                if (document.getElementById((i)+"_"+(j+2)).innerHTML == ""){
                                    document.getElementById((i)+"_"+(j+2)).click();
                                    return true;
                                }
                            }     
                        }
                        else if (document.getElementById((i)+"_"+(j+1)).innerHTML == "" && b == 1){
                                document.getElementById((i)+"_"+(j+1)).click();
                                return true;
                        }
                    }
                }
            }
        }
    } 
}